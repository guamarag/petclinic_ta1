/*
 * Copyright 2016-2017 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.springframework.samples.petclinic.vet;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.samples.petclinic.visit.Visit;
import org.springframework.util.SerializationUtils;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * @author Dave Syer
 *
 */
public class VetTests {

	public static Vet vet;
	
	@BeforeClass
	//This method is called before executing this suite
	public static void initClass() {
		vet = new Vet();
		vet.setFirstName("Juan Manuel");
		vet.setLastName("Murillo");
	}
	
	@Test
	public void testHomeVisitsFalse() {
		vet.setHomeVisits(false);
		assertFalse(vet.getHomeVisits().booleanValue());
	}
	
	@Test
	public void testHomeVisitsTrue() {
		vet.setHomeVisits(true);
		assertTrue(vet.getHomeVisits().booleanValue());
	}
	
	@Test
	public void testAddingVisits() {
		Visit v=new Visit();
		v.setDescription("x");
		v.setPetId(123);
		vet.addVisit(v);
		assertEquals(vet.getVisits().size(), 1);
		assertEquals(vet.getVisits().get(0).getPetId().intValue(), 123);
	}
	
	@Test
	public void specialtyTest() {
		Specialty s=new Specialty();
		s.setId(123);
		s.setName("Dogs");
		vet.addSpecialty(s);
		assertEquals(vet.getSpecialties().get(0).getId().intValue(),123);
		assertEquals(vet.getSpecialties().get(0).getName(),"Dogs");
	}
	
    @Test
    public void testSerialization() {
        Vet vet = new Vet();
        vet.setFirstName("Zaphod");
        vet.setLastName("Beeblebrox");
        vet.setId(123);
        Vet other = (Vet) SerializationUtils
                .deserialize(SerializationUtils.serialize(vet));
        assertThat(other.getFirstName()).isEqualTo(vet.getFirstName());
        assertThat(other.getLastName()).isEqualTo(vet.getLastName());
        assertThat(other.getId()).isEqualTo(vet.getId());
    }

}
