package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.samples.petclinic.visit.Visit;

public class PetTests {
	
	public static Pet pet;
	public static Pet pet2;
	
	@BeforeClass
	public static void initPet() {
		pet = new Pet();
		pet.setId(1234);
		pet.setName("Lucy");
		pet.setComments("Test 1. Add comment into a pet");
		pet.setWeight(7);
	}
	
	@Test
	public void testVisits() {
		Visit visit = new Visit();
		visit.setDescription("vaccine");
		visit.setId(456);
		pet.addVisit(visit);
		assertEquals(1, pet.getVisits().size());
		assertEquals(pet.getId().intValue(), pet.getVisits().get(0).getPetId().intValue());
		assertEquals(visit.getDescription(), pet.getVisits().get(0).getDescription());
		visit.setDescription("shots");
		assertEquals(visit.getDescription(), pet.getVisits().get(0).getDescription());
	}
	
	@Test
	public void nameTest() {
		assertNotNull(pet.getName());
		assertNotEquals(pet.getName(),"LuCy");
		assertEquals(pet.getName(),"Lucy");
		pet.setName("Peter");
		assertNotNull(pet.getName());
		assertNotEquals(pet.getName(),"PEteR");
		assertEquals(pet.getName(),"Peter");
	}
	
		@Test
	public void testPet() {
		assertNotNull(pet);
	}
	
	@Test
	public void testPet2() {
		assertNull(pet2);
	}
	
	@Test
	public void testComments() {
		assertNotNull(pet.getComments());
		assertNotEquals(pet.getComments(), "Test 1");
		assertEquals(pet.getComments(), "Test 1. Add comment into a pet");
	}
	
	@Test
	public void testWeight() {
		assertNotNull(pet.getWeight());
		assertEquals(pet.getWeight(), 7, 0.0f);
		assertNotEquals(pet.getWeight(), 1);
	}
	
	
	@Ignore
	public void ignore() {
		pet.setWeight(1);
	}
	
	@After
	public void finish() {
		pet.setComments("Test 1. Add comment into a pet");
	}
	
	
	@AfterClass
	public static void finishClass() {
		pet = null;
	}

}
