package org.springframework.samples.petclinic.owner;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class PetRepositoryTests {
	
	
	@Autowired
	public PetRepository repo;
	
	@Autowired
	public OwnerRepository orepo;
	
	public Pet pet;
	
	@Before
	public void initRepository() {
		Owner owner = new Owner();
		owner.setFirstName("Aaron");
		owner.setLastName("Lopez");
		owner.setAddress("Caceres");
		owner.setCity("Spain");
		owner.setTelephone("1234567");
		orepo.save(owner);
		pet = new Pet();
		pet.setName("Bobby");
		pet.setOwner(owner);
		pet.setType(repo.findPetTypes().get(0));
		repo.save(pet);
	}
	
	@Test
	public void findPetsByNameTest() {
		
		Owner o=new Owner();
		o.setAddress("C/");
		o.setCity("Madrid");
		o.setFirstName("Pedro");
		o.setLastName("Perez");
		o.setTelephone("666");
		orepo.save(o);
		
		Pet p=new Pet();
		p.setComments("x");
		p.setName("Tom");
		p.setOwner(o);
		p.setType(repo.findPetTypes().get(0));
		p.setWeight(100);
		repo.save(p);
		
		assertNotNull(repo.findPetsByName(p.getName()));
		assertTrue(repo.findPetsByName(p.getName()).size()>0);
	}
	
	@Test
	public void testDelete() {
		assertNotNull(repo.findById(pet.getId()));
		repo.delete(pet);
		assertNull(repo.findById(pet.getId()));
	}

}
