package org.springframework.samples.petclinic.vet;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;



@RunWith(SpringRunner.class)
@DataJpaTest(includeFilters = @ComponentScan.Filter(Repository.class))
public class VetRepositoryTests {

	@Autowired
	private VetRepository vets;
	
	private Vet vet;
	
	@Test
	public void init() {
		vet = new Vet();
		vet.setId(50);
		vet.setFirstName("Miss");
		vet.setLastName("Orange");
		vet.setHomeVisits(false);
		
		
		Collection <Specialty> vetSpec = this.vets.findSpecialties();
		
		if(!vetSpec.isEmpty()) {
			for (Specialty spec : vetSpec) {
				assertNotNull(spec);
			}
		}
	}
	

	@After
	public void finish() {
		this.vet=null;
	}

}
