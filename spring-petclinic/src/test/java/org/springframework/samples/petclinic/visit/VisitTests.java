package org.springframework.samples.petclinic.visit;

import static org.junit.Assert.*;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.samples.petclinic.vet.Vet;

public class VisitTests {
	public static Visit visit;

	@BeforeClass
	// This method is called before executing this suite
	public static void initClass() {
		visit = new Visit();
		visit.setId(123);
		visit.setDescription("x");
		visit.setPetId(456);
	}
	
	@Test
	public void testAddingVets() {
		Vet v=new Vet();
		v.setFirstName("Juan Manuel");
		v.setLastName("Murillo");
		visit.setVet(v);
		assertEquals(visit.getVet().getFirstName(), "Juan Manuel");
		assertEquals(visit.getVet().getLastName(), "Murillo");
	}

}
